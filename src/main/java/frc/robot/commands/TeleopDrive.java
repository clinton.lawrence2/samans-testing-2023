package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;  


public class TeleopDrive extends CommandBase {
  private final DriveSubsystem driveSub;
  private final DoubleSupplier forward;
  private final DoubleSupplier turn;

  /**
   * Our Default Human Controlled Driving Creates a new TeleopDrive.
   *
   * Takes in values from the joysticks (y and x axis), sets them to local variables (forward and turn turn) and sends them to the Drivesubsystems "rampDrive Function"
   */
  public TeleopDrive(DoubleSupplier y, DoubleSupplier x , DriveSubsystem m_robotDrive) {
    driveSub = m_robotDrive;
    forward = y;
    turn = x;
    addRequirements(driveSub);
  }

@Override
  public void execute() 
  {
      driveSub.rampDrive(forward.getAsDouble(), turn.getAsDouble());
  }

  //Note this has no "isFinished" as we want this command to always be executed during teleop
}