package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Arm;

public class MoveDown extends CommandBase {

    Arm arm;

    /* 
     * This command will Move our arm "down" (towards front of the robot)
     */

    // we take the instance of the Arm created in RobotContainer (which this function calls "armies") and save it to a local  variable reference this class has called "arm"

    public MoveDown(Arm armies)
    {
        arm = armies;
    }
        
     // Move the Arm a set speed
    @Override
    public void execute() 
    {
        arm.moveArm(Constants.ARM_MOTOR_SPEED);
    }
    
    //Let the Command Scheduler (things that runs the 20ms loops) know this command is done as soon as it runs
    @Override
    public boolean isFinished() 
    {
        return true;
    }
        
    }    

