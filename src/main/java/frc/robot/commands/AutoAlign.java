package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.DriveSubsystem; 

public class AutoAlign extends CommandBase
{
    
    /* 
     * This command will auto align the robot using the limelight camera
     * The idea is to get the target (Reflective Light,AprilTag, as close to the center of the camera's picture as possible)
     */

    private final DriveSubsystem m_drive;
    private final Camera limelight; 
    double turn = 0; // turn speed to give the motors


    public AutoAlign(DriveSubsystem driveSub, Camera lime)
    {
        m_drive = driveSub;
        limelight = lime;

    }

    @Override
    public void end(boolean interrupted)
    {
        //when we hit "center" send 0 power to motors aka stop
        m_drive.directDrive(0, 0);

    }

    @Override
    public void execute() 
    {
        /* Use a PID controller to decide how much to turn the robot by. 
         * The "offCenter" is rechecked every ~20ms
         * We take a proportionate value(P) of the offCenter and use that to turn towards the target
         * 
         * ie If the off center is 20 degrees we take 1.5% of offcenter(.3) and turn by that value (in this case send 30% power to the motor)
         * This value is calculated and saved to our variable named "turn"
         * 
         * As the turn happens the offCenter will update and the calculation will happen again and adjust the power sent to the motor
         * There is a point where the motor power we want to send is too large which could cause us to spin too fast and lose the target
         * We counter this by using a constant called "AUTO_ALIGN_MAX_TURN_SPEED"
         * If our turn value is outside this constant we set our turn to this value
         *  
         */

        double offCenter = limelight.getHorizontalOffCenter();
        //PID VALUES Porportional,Intergral,Derivitive
        double P = 0.015; //How fast we move towards the target
        
        turn = -(offCenter*P);

        if(turn > 0 && turn < Constants.AUTO_ALIGN_MAX_TURN_SPEED) // our turn spin is greater than our max
        {
            turn = Constants.AUTO_ALIGN_MAX_TURN_SPEED;
        }
        else if(turn < 0 && turn > -Constants.AUTO_ALIGN_MAX_TURN_SPEED) // our turn speed is "less than" our max speed in the other direction
        {
            turn = -Constants.AUTO_ALIGN_MAX_TURN_SPEED;
        }

        SmartDashboard.putNumber("ALIGN X", turn);
        m_drive.directDrive(0, turn);

    }
    @Override
    public boolean isFinished() {
        return limelight.isCenter();

    }
}