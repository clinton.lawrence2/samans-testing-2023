package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.DriveSubsystem; 

public class AutoDriveTime extends CommandBase{
    private final DriveSubsystem driveSub;
    private final double speedLocal;
    private final double howManyLoops;
    private double currentLoopCounts;

    /* 
     * This command will drive the robot for a set amount of time (in seconds)
     * We do this by setting a counter and counting how many "loops" we go through before stopping
     * Remember all code runs within a loop that is ran every ~20ms 
     */

    public AutoDriveTime(double speed, DriveSubsystem driveSubsystem)
    {
        speedLocal = speed;
        driveSub = driveSubsystem;
        howManyLoops = (Constants.AUTO_DRIVE_TIME_MAX*1000) / 20;
        currentLoopCounts = 0;//use this variable to keep track of how many loops we have ran

    }

    @Override
    public void end(boolean interrupted)
    {
        //whe have ran this code for the number of loops (howManyLoops) send 0 power to motors aka stop
        driveSub.directDrive(0, 0);
    }

    @Override
    public void execute() // set the motor to the speed while running this loop
    {
        driveSub.directDrive(speedLocal, 0);
        currentLoopCounts++;

    }
    @Override
    public boolean isFinished() // check if we have hit the number of loops we requested, if not then keep running this command
    {
        return (currentLoopCounts >= howManyLoops);
    }
}