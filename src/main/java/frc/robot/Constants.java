/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide
 * numerical or boolean constants. This class should not be used for any other
 * purpose. All constants should be declared globally (i.e. public static). Do
 * not put anything functional in this class.
 *
 * <p>
 * It is advised to statically import this class (or one of its inner classes)
 * wherever the constants are needed, to reduce verbosity.
 */
public final class Constants {
    /* Current threshold to trigger current limit */
    public static final int PEAK_MOTOR_AMPS = 40;


    //Auto Drive
    public static double AUTO_DRIVE_TIME_MAX = 10.0; // in seconds

    //Auto Camera align
    public static double AUTO_ALIGN_DEADBAND = .3; //how close to center does the target need to be before finishing aligning
    public static double AUTO_ALIGN_MAX_TURN_SPEED = .3; //how fast do we want the motor to go

    //Camera
    public static double TARGET_CENTER_DEADBAND = 5;

    //Motor speeds
    public static double DRIVE_MOTOR_MAX_FWD = .3;
    public static double DRIVE_MOTOR_MAX_BKWD = -.3;
    public static double AUTO_DRIVE_SPEED = 1.0;
    
    //Motor Numbers
    public static int LEFT_DRIVE_MOTOR_LEADER_CAN_ID = 3;
    public static int RIGHT_DRIVE_MOTOR_LEADER_CAN_ID = 4;
    public static int LEFT_DRIVE_MOTOR_FOLLOW1_CAN_ID = 1;
    public static int LEFT_DRIVE_MOTOR_FOLLOW2_CAN_ID = 2;
    public static int RIGHT_DRIVE_MOTOR_FOLLOW1_CAN_ID = 5;
    public static int RIGHT_DRIVE_MOTOR_FOLLOW2_CAN_ID = 6;

    //Drive Controler
    public static int DRIVE_CONTROL_PORT = 0;
    public static int DRIVE_Y_AXIS = 1;
    public static int DRIVE_X_AXIS = 4;

    //Operator Controller
    public static int OPERATOR_CONTROL_PORT = 1;
    public static int LEFT_TRIGGER_AXIS = 2;
    public static int RIGHT_TRIGGER_AXIS = 3;
    public static int SIM_LEFT_TRIGGER_AXIS = 4;
    public static int SIM_RIGHT_TRIGGER_AXIS = 5;

    //Claw
    public static int CLAW_SOLENOID_PORT = 1; // port the solenoid is plugged into on the PCM

    //Arm
    public static double ARM_MOTOR_SPEED = 1.0;
    public static int ARM_MOTOR_CAN_ID = 1;
    public static int ARM_SOFT_LIMIT_FWD = 15; //encoder position 
    public static int ARM_SOFT_LIMIT_BKWD = 0; //encoder position
    public static boolean ENABLE_ENCODER_LIMITS = true; // enable encoder based position limiting

}
