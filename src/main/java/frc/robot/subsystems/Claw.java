package frc.robot.subsystems;

import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.Solenoid;
import frc.robot.Constants;

public class Claw 
{

    /*
     * This is our claw subsystem
     * The claw opens and closes using a solenoid 
     * A solenoid is an electrical switch for open and closing a valve to let air pass through
     * To define the solenoid which company's PCM (Pneumatics Control Module we are using)
     * And which port on that PCM we are using
     */
Solenoid clawSolenoid = new Solenoid(PneumaticsModuleType.CTREPCM, Constants.CLAW_SOLENOID_PORT);

public void openClaw()
{
    clawSolenoid.set(true); // TODO boolean may need to flipped to false get desired result
}


public void closeClaw()
{
    clawSolenoid.set(false); // TODO boolean may need to flipped to true get desired result
}


}
