package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Camera extends SubsystemBase
{
    NetworkTable limelight;
    boolean centered;
    /*
     * Subsystem for our limelight camera
     * The lime light camera works by reading the "network table" called "limelight"
     * Network tables are a messaging queue that our limelight camera writes data too.
     * We can look at this data using "keys" within the messages
     * The limelight returns many values the ones we care about right now are 
     *     how off center the target is (shown with tx and ty)
     *     does the camera even see a target (shown with the "tv" key)
     *      is the target cented in the camera's picture
     * 
     * For detecting if center we set a threshold of within about 5 degrees.
     */
    public Camera() 
    {
        super();
        limelight = NetworkTableInstance.getDefault().getTable("limelight");
    }

    public double getHorizontalOffCenter()
    {
        return limelight.getEntry("tx").getDouble(0);
    }

    public double getVerticalOffCenter()
    {
        return limelight.getEntry("ty").getDouble(0);
    }
    public boolean doWeHaveATarget()
    {
        return limelight.getEntry("tv").getBoolean(false);
    }

    public boolean isCenter()
    {
        centered = (getHorizontalOffCenter() < Constants.TARGET_CENTER_DEADBAND && getHorizontalOffCenter() > -Constants.TARGET_CENTER_DEADBAND);
        return centered;
    }
}
