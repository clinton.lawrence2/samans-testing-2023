/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.button.CommandXboxController;
import edu.wpi.first.wpilibj2.command.button.Trigger;
import frc.robot.commands.AutoAlign;
import frc.robot.commands.CloseClaw;
import frc.robot.commands.MoveDown;
import frc.robot.commands.MoveUp;
import frc.robot.commands.OpenClaw;
import frc.robot.commands.TeleopDrive;
import frc.robot.subsystems.Arm;
import frc.robot.subsystems.Camera;
import frc.robot.subsystems.Claw;
import frc.robot.subsystems.DriveSubsystem;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {


  // The robot's subsystems and commands are defined here... 

  
  private final DriveSubsystem m_robotDrive = new DriveSubsystem();
  private final Camera camera = new Camera();
  private final CommandXboxController  driveJoyStick = new CommandXboxController (Constants.DRIVE_CONTROL_PORT);
  private final CommandXboxController  armJoyStick = new CommandXboxController(Constants.OPERATOR_CONTROL_PORT);
  private final Arm arm = new Arm();
  private final Claw claw = new Claw();



  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() 
  {
    configureButtonBindings();
    m_robotDrive.setDefaultCommand( new TeleopDrive(() -> driveJoyStick.getRawAxis(Constants.DRIVE_Y_AXIS),() -> driveJoyStick.getRawAxis(Constants.DRIVE_X_AXIS), m_robotDrive));
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.Trigger}.
   */
  private void configureButtonBindings() {
  
    final Trigger driverAButton = driveJoyStick.a();
    final Trigger driverXButton = driveJoyStick.x();
    final Trigger driverYButton = driveJoyStick.y();
    
    final Trigger armAButton = armJoyStick.a();

    armAButton.whileTrue(new OpenClaw(claw));
    armAButton.whileTrue(new CloseClaw(claw));

    
    driverAButton.onTrue(new AutoAlign(m_robotDrive,camera));
    driverXButton.whileTrue(new MoveDown(arm));
    driverYButton.whileTrue(new MoveUp(arm));
    
    SmartDashboard.putData(m_robotDrive);

    
  }

  public Command getAutonomousCommand() {
    return null;
  }


}
